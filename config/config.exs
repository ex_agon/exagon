# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :exagon,
  ecto_repos: [Exagon.Repo],
  generators: [binary_id: true]

config :exagon, Exagon.Repo,
  migration_primary_key: [name: :id, type: :binary_id],
  migration_timestamps: [type: :utc_datetime]

# Configures the endpoint
config :exagon, ExagonWeb.Endpoint,
  url: [host: "localhost"],
  adapter: Bandit.PhoenixAdapter,
  render_errors: [
    formats: [json: ExagonWeb.ErrorJSON],
    layout: false
  ],
  pubsub_server: Exagon.PubSub,
  live_view: [signing_salt: "0T8Gv5s0"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :exagon, Matrix,
  interactive_auth: %{
    "POST /_matrix/client/v3/register" => %{
      "flows" => [%{"stages" => ["m.login.dummy"]}]
    },
    "POST /_matrix/client/r0/register" => %{
      "flows" => [%{"stages" => ["m.login.dummy"]}]
    },
    "GET /_matrix/client/r0/register/available" => %{
      "flows" => [%{"stages" => ["m.login.dummy"]}]
    }
  },
  registration: [enabled: true]

config :exagon, Exagon.Users.AuthTokenCache, backend: :shards

config :exagon, Exagon.Users.InteractiveAuthSessionCache,
  backend: :shards,
  max_size: 1000,
  ttl: :timer.minutes(10),
  gc_interval: :timer.hours(1),
  gc_cleanup_min_timeout: :timer.seconds(10),
  gc_cleanup_max_timeout: :timer.minutes(10)

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
