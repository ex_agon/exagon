import Config
require Logger
alias Exagon.Config.Transformer

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.
# The block below contains prod specific runtime configuration.

# ## Using releases
#
# If you use `mix release`, you need to explicitly enable the server
# by passing the PHX_SERVER=true when you start it:
#
#     PHX_SERVER=true bin/exagon start
#
# Alternatively, you can use `mix phx.gen.release` to generate a `bin/server`
# script that automatically sets the env var above.
if System.get_env("PHX_SERVER") do
  config :exagon, ExagonWeb.Endpoint, server: true
end

if config_env() == :prod do
  config_file = System.get_env("EXAGON_CONFIG") || Path.join(File.cwd!(), "config.yaml")
  Logger.debug("Exagon configuration file: #{config_file}")

  yaml_config =
    case YamlElixir.read_from_file(config_file) do
      {:ok, config} ->
        config

      {:error, %YamlElixir.FileNotFoundError{} = error} ->
        Logger.error(error.message)
        raise error
    end

  try do
    Logger.configure(level: Transformer.get_in!(yaml_config, ["log_level"], :atom?))
    database_url = Transformer.get_in!(yaml_config, ["database", "url"], :string)
    pool_size = Transformer.get_in!(yaml_config, ["database", "pool_size"], :integer?) || 10

    config :exagon, Exagon.Repo,
      url: database_url,
      pool_size: pool_size

    # The secret key base is used to sign/encrypt cookies and other secrets.
    # A default value is used in config/dev.exs and config/test.exs but you
    # want to use a different value for prod and you most likely don't want
    # to check this value into version control, so we use an environment
    # variable instead.
    secret_key_base =
      System.get_env("SECRET_KEY_BASE") ||
        raise """
        environment variable SECRET_KEY_BASE is missing.
        You can generate one by calling: mix phx.gen.secret
        """

    server_name = Transformer.get_in!(yaml_config, ["server_name"], :string!)
    listener = Transformer.get_in!(yaml_config, ["listener"], &Transformer.to_bandit_options/1)

    case listener[:scheme] do
      :http ->
        config :exagon, ExagonWeb.Endpoint,
          url: [host: server_name],
          http: listener,
          secret_key_base: secret_key_base

      :https ->
        config :exagon, ExagonWeb.Endpoint,
          url: [host: server_name],
          https: listener,
          cipher_suite: :strong,
          force_ssl: [hsts: true],
          secret_key_base: secret_key_base
    end

    registration_enabled =
      Transformer.get_in!(yaml_config, ["registration", "enabled"], :boolean?) || false

    config :exagon, Matrix,
      server_name: server_name,
      registration: [enabled: registration_enabled]
  rescue
    e in Exagon.Config.Error ->
      Logger.error("Invalid config value at path '#{inspect(e.path)}': #{e.message}")
      :init.stop()
      reraise e, __STACKTRACE__
  end

  # ## SSL Support
  #
  # To get SSL working, you will need to add the `https` key
  # to your endpoint configuration:
  #
  #     config :exagon, ExagonWeb.Endpoint,
  #       https: [
  #         ...,
  #         port: 443,
  #         cipher_suite: :strong,
  #         keyfile: System.get_env("SOME_APP_SSL_KEY_PATH"),
  #         certfile: System.get_env("SOME_APP_SSL_CERT_PATH")
  #       ]
  #
  # The `cipher_suite` is set to `:strong` to support only the
  # latest and more secure SSL ciphers. This means old browsers
  # and clients may not be supported. You can set it to
  # `:compatible` for wider support.
  #
  # `:keyfile` and `:certfile` expect an absolute path to the key
  # and cert in disk or a relative path inside priv, for example
  # "priv/ssl/server.key". For all supported SSL configuration
  # options, see https://hexdocs.pm/plug/Plug.SSL.html#configure/1
  #
  # We also recommend setting `force_ssl` in your endpoint, ensuring
  # no data is ever sent via http, always redirecting to https:
  #
  #     config :exagon, ExagonWeb.Endpoint,
  #       force_ssl: [hsts: true]
  #
  # Check `Plug.SSL` for all available options in `force_ssl`.
end
