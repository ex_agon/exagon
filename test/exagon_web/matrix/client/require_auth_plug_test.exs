defmodule ExagonWeb.Matrix.Client.RequireAuthPlugTest do
  use ExagonWeb.ConnCase
  import Exagon.Factory
  alias Polyjuice.Util.ClientAPIErrors

  setup do
    user = insert(:user)
    device = insert(:device, user: user)
    account = insert(:account, user: user)
    token = insert(:access_token, device: device)
    {:ok, %{user: user, device: device, account: account, token: token}}
  end

  test "fetch access token with request header", %{conn: conn, token: token} do
    conn =
      put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
      |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)

    assert conn.assigns[:access_token_value] == token.value
  end

  test "fetch access token with request parameter", %{conn: conn, token: token} do
    conn =
      get(conn, ~p"/_matrix/client/v3/login?access_token=#{token.value}")
      |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)

    assert conn.assigns[:access_token_value] == token.value
  end

  test "fetch access token with no token", %{conn: conn} do
    error =
      assert_raise ExagonWeb.MatrixError, fn ->
        ExagonWeb.Matrix.Client.RequireAuthPlug.call(conn, nil)
      end

    assert %{plug_status: 403, matrix_error: ClientAPIErrors.MMissingToken} = error
  end

  test "find user info", %{conn: conn, user: user, device: device, account: account, token: token} do
    conn =
      put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
      |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)

    assert %{user: u, account: a, device: d, access_token: t} = conn.assigns[:user_info]
    assert user.id == u.id
    assert device.id == d.id
    assert account.id == a.id
    assert token.id == t.id
  end

  test "find user info with unkown token", %{conn: conn} do
    error =
      assert_raise ExagonWeb.MatrixError, fn ->
        put_req_header(conn, "authorization", <<"Bearer UNKNOWN">>)
        |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)
      end

    assert %{plug_status: 403, matrix_error: ClientAPIErrors.MUnknownToken} = error
  end

  test "validate access_token", %{
    conn: conn,
    user: user,
    device: device,
    account: account,
    token: token
  } do
    conn =
      put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
      |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)

    assert %{user: u, account: a, device: d, access_token: t} = conn.assigns[:user_info]
    assert user.id == u.id
    assert device.id == d.id
    assert account.id == a.id
    assert token.id == t.id
  end

  test "validate soft_logout access_token", %{
    conn: conn,
    device: device
  } do
    token = insert(:access_token, device: device, soft_logout: true)

    error =
      assert_raise ExagonWeb.MatrixError, fn ->
        put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
        |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)
      end

    assert %{plug_status: 403, matrix_error: ClientAPIErrors.MUnknownToken} = error
  end

  test "validate expired access_token", %{
    conn: conn,
    device: device
  } do
    token =
      insert(:access_token,
        device: device,
        expires_at: DateTime.utc_now() |> DateTime.add(-1, :minute)
      )

    error =
      assert_raise ExagonWeb.MatrixError, fn ->
        put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
        |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)
      end

    assert %{plug_status: 403, matrix_error: ClientAPIErrors.MUnknownToken} = error
  end
end
