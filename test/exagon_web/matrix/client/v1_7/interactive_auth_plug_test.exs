defmodule ExagonWeb.Matrix.Client.V17.InteractiveAuthPlugTest do
  use ExagonWeb.ConnCase

  describe "User-interactive API" do
    test "reply first with new session", %{conn: conn} do
      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/register", ~s"{\"test\": true}")

      assert conn.status == 401
      assert {:ok, flow} = Jason.decode(conn.resp_body)
      assert %{"session" => _session} = flow
    end

    test "call with invalid json return error", %{conn: conn} do
      assert_raise Plug.Parsers.ParseError, fn ->
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/register", "{")
      end
    end

    test "call with invalid session", %{conn: conn} do
      request = %{"auth" => %{"session" => "00000", "type" => "m.login.dummy"}}

      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/register", request)

      assert conn.status == 400
      assert {:ok, flow} = Jason.decode(conn.resp_body)
      assert %{"errcode" => errcode, "error" => _error} = flow
      assert errcode == "M_BAD_JSON"
    end

    test " call with invalid auth type", %{conn: conn} do
      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/register", ~s"")

      assert conn.status == 401
      assert {:ok, flow} = Jason.decode(conn.resp_body)
      assert %{"session" => session} = flow
      request = %{"auth" => %{"session" => session, "type" => "m.login.invalid"}}

      conn =
        Phoenix.ConnTest.build_conn()
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/register", request)

      assert conn.status == 401
      assert {:ok, flow} = Jason.decode(conn.resp_body)
      assert %{"errcode" => errcode, "error" => _error} = flow
      assert errcode == "M_FORBIDDEN"
    end

    test "/register call with m.login.dummy auth", %{conn: conn} do
      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/register", ~s"")

      assert conn.status == 401
      assert {:ok, flow} = Jason.decode(conn.resp_body)
      assert %{"session" => session} = flow
      request = %{"auth" => %{"session" => session, "type" => "m.login.dummy"}}

      Phoenix.ConnTest.build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/_matrix/client/v3/register?kind=guest", request)

      # Add success assertions
    end
  end
end
