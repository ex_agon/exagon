defmodule ExagonWeb.Matrix.Client.V17.Controllers.RegisterControllerTest do
  use ExagonWeb.ConnCase
  import Exagon.Factory
  alias ExagonWeb.Matrix.Client.V17.Controllers.RegisterController.RegisterRequest
  alias Polyjuice.Util.ClientAPIErrors

  describe "RegisterRequest parameters" do
    test "generate missing default parameters" do
      assert {:ok, request} = RegisterRequest.from_params(%{})
      assert request.device_id != ""
      assert request.username != ""
    end
  end

  describe "POST /_matrix/client/v3/register" do
    test "replies 200 with nominal parameters" do
      conn = post_interactive_auth("/_matrix/client/v3/register?kind=user", %{})
      assert {:ok, response} = Jason.decode(conn.resp_body)

      assert %{
               "device_id" => _device_id,
               "refresh_token" => _refresh_token,
               "access_token" => _access_token,
               "user_id" => _user_id,
               "expires_in_ms" => _expires_in_ms
             } = response
    end

    test "replies 200 with inhibit_login" do
      conn =
        post_interactive_auth("/_matrix/client/v3/register?kind=user", %{"inhibit_login" => true})

      assert {:ok, response} = Jason.decode(conn.resp_body)

      assert %{
               "user_id" => _user_id
             } = response
    end

    test "replies 400 with invalid kind" do
      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       post_interactive_auth("/_matrix/client/v3/register?kind=other", %{})
                     end

      assert %{plug_status: 400, matrix_error: ClientAPIErrors.MBadJson} = error
    end

    test "replies 400 with user in use" do
      user = insert(:user)
      {:ok, mx_user_id} = Polyjuice.Util.Identifiers.V1.UserIdentifier.parse(user.mx_user_id)

      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       post_interactive_auth("/_matrix/client/v3/register?kind=user", %{
                         username: mx_user_id.localpart
                       })
                     end

      assert %{plug_status: 400, matrix_error: ClientAPIErrors.MUserInUse} = error
    end

    test "replies 400 with invalid username" do
      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       post_interactive_auth("/_matrix/client/v3/register?kind=user", %{
                         username: "@@"
                       })
                     end

      assert %{plug_status: 400, matrix_error: ClientAPIErrors.MInvalidUsername} = error
    end
  end

  defp post_interactive_auth(endpoint, request) do
    conn =
      Phoenix.ConnTest.build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post(endpoint, Jason.encode!(request))

    assert conn.status == 401
    assert {:ok, flow} = Jason.decode(conn.resp_body)
    assert %{"session" => session} = flow
    request = Map.merge(request, %{"auth" => %{"session" => session, "type" => "m.login.dummy"}})

    Phoenix.ConnTest.build_conn()
    |> put_req_header("accept", "application/json")
    |> put_req_header("content-type", "application/json")
    |> post(endpoint, Jason.encode!(request))
  end
end
