defmodule ExagonWeb.Matrix.Client.V17.Controllers.KeysControllerTest do
  use ExagonWeb.Matrix.Client.AuthCase

  describe "POST /_matrix/client/v3/keys/upload" do
    test "upload device_key",
         %{conn: conn, device_id: device_id, user_id: user_id} do
      {public_key, private_key} = :crypto.generate_key(:eddsa, :ed25519)

      {:ok, device_keys} =
        %{
          "algorithms" => [
            "m.olm.v1.curve25519-aes-sha2",
            "m.megolm.v1.aes-sha2"
          ],
          "device_id" => device_id,
          "keys" => %{
            "ed25519:#{device_id}" => Base.encode64(public_key, padding: false)
          },
          "user_id" => user_id
        }
        |> Polyjuice.Util.JSON.sign(
          user_id,
          %Polyjuice.Util.Ed25519.SigningKey{key: private_key, id: device_id}
        )

      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/keys/upload", %{
          "device_keys" => device_keys
        })

      assert conn.status == 200
    end

    test "upload device_key fails with invalid signature",
         %{conn: conn, device_id: device_id, user_id: user_id} do
      {public_key, _} = :crypto.generate_key(:eddsa, :ed25519)
      {_, wrong_private_key} = :crypto.generate_key(:eddsa, :ed25519)

      {:ok, device_keys} =
        %{
          "algorithms" => [
            "m.olm.v1.curve25519-aes-sha2",
            "m.megolm.v1.aes-sha2"
          ],
          "device_id" => device_id,
          "keys" => %{
            "ed25519:#{device_id}" => Base.encode64(public_key, padding: false)
          },
          "user_id" => user_id
        }
        |> Polyjuice.Util.JSON.sign(
          user_id,
          %Polyjuice.Util.Ed25519.SigningKey{key: wrong_private_key, id: device_id}
        )

      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       conn
                       |> put_req_header("accept", "application/json")
                       |> put_req_header("content-type", "application/json")
                       |> post("/_matrix/client/v3/keys/upload", %{
                         "device_keys" => device_keys
                       })
                     end

      assert %{plug_status: 400, matrix_error: Polyjuice.Util.ClientAPIErrors.MBadJson} = error
    end
  end
end
