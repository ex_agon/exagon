defmodule ExagonWeb.Matrix.Client.V17.Controllers.LoginControllerTest do
  use ExagonWeb.ConnCase
  import Exagon.Factory
  alias Polyjuice.Util.ClientAPIErrors

  describe "POST /_matrix/client/v3/login" do
    test "replies 200 for allowed user with new device", %{conn: conn} do
      user = insert(:user)
      account = insert(:account, user: user, password: "password")

      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/login", %{
          "type" => "m.login.password",
          "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
          "password" => account.password
        })

      assert conn.status == 200
      assert {:ok, response} = Jason.decode(conn.resp_body)

      %{
        "access_token" => access_token,
        "device_id" => device_id,
        "expires_in_ms" => _expires_in_ms,
        "refresh_token" => _refresh_token,
        "user_id" => user_id,
        "well_known" => _well_known
      } = response

      assert user_id == user.mx_user_id

      {_, _, device, _} =
        Exagon.Users.find_user_info_from_access_token(access_token)

      assert device_id == device.mx_device_id
    end

    test "replies 200 for allowed user with existing device", %{conn: conn} do
      user = insert(:user)
      account = insert(:account, user: user, password: "password")
      device = insert(:device, user: user)

      conn =
        conn
        |> put_req_header("accept", "application/json")
        |> put_req_header("content-type", "application/json")
        |> post("/_matrix/client/v3/login", %{
          "type" => "m.login.password",
          "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
          "password" => account.password,
          "device_id" => device.mx_device_id
        })

      assert conn.status == 200
      assert {:ok, response} = Jason.decode(conn.resp_body)

      %{
        "access_token" => access_token_value,
        "device_id" => device_id,
        "expires_in_ms" => _expires_in_ms,
        "refresh_token" => _refresh_token,
        "user_id" => user_id,
        "well_known" => _well_known
      } = response

      {_, _, _, access_token} =
        Exagon.Users.find_user_info_from_access_token(access_token_value)

      assert user_id == user.mx_user_id
      assert device_id == device.mx_device_id
      assert access_token_value == access_token.value
    end

    test "replies 403 for invalid user password", %{conn: conn} do
      user = insert(:user)
      _account = insert(:account, user: user, password: "password")

      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       conn
                       |> put_req_header("accept", "application/json")
                       |> put_req_header("content-type", "application/json")
                       |> post("/_matrix/client/v3/login", %{
                         "type" => "m.login.password",
                         "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
                         "password" => "WRONG PASSWORD"
                       })
                     end

      assert %{plug_status: 403, matrix_error: ClientAPIErrors.MForbidden} = error
    end

    test "replies 400 with invalid login type", %{conn: conn} do
      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       conn
                       |> put_req_header("accept", "application/json")
                       |> put_req_header("content-type", "application/json")
                       |> post("/_matrix/client/v3/login", %{"type" => "test"})
                     end

      assert %{plug_status: 400, matrix_error: ClientAPIErrors.MBadJson} = error
    end

    test "replies 400 without login type", %{conn: conn} do
      error =
        assert_raise ExagonWeb.MatrixError,
                     fn ->
                       conn
                       |> put_req_header("accept", "application/json")
                       |> put_req_header("content-type", "application/json")
                       |> post("/_matrix/client/v3/login", %{})
                     end

      assert %{plug_status: 400, matrix_error: ClientAPIErrors.MBadJson} = error
    end
  end
end
