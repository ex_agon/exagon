defmodule ExagonWeb.Matrix.Client.ConnUtilsTest do
  use ExagonWeb.ConnCase
  import Exagon.Factory

  test "get_user_id", %{conn: conn} do
    user = insert(:user)
    device = insert(:device, user: user)
    _account = insert(:account, user: user)
    token = insert(:access_token, device: device)

    assert user.mx_user_id ==
             put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
             |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)
             |> ExagonWeb.Matrix.Client.ConnUtils.get_user_id()
  end

  test "get_device_id", %{conn: conn} do
    user = insert(:user)
    device = insert(:device, user: user)
    _account = insert(:account, user: user)
    token = insert(:access_token, device: device)

    assert device.mx_device_id ==
             put_req_header(conn, "authorization", <<"Bearer #{token.value}">>)
             |> ExagonWeb.Matrix.Client.RequireAuthPlug.call(nil)
             |> ExagonWeb.Matrix.Client.ConnUtils.get_device_id()
  end
end
