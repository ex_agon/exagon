defmodule Exagon.Users.UsersTest do
  use Exagon.DataCase
  import Exagon.Factory

  describe "user availability" do
    test "user available" do
      assert {:ok, true} = Exagon.Users.is_username_available("abcdefgh")
    end

    test "user not available" do
      user = insert(:user)
      _account = insert(:account, user: user)
      assert {:ok, false} = Exagon.Users.is_username_available(user.mx_user_id)
    end
  end
end
