defmodule Exagon.Utils.UaTest do
  use ExUnit.Case

  describe "device info parsing" do
    test "works with list" do
      result =
        [
          {"user-agent",
           "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/116.0"},
          {"ip-address", {127, 0, 0, 1}}
        ]
        |> Exagon.Utils.UA.parse()

      assert %UAInspector.Result{} = result
    end

    test "works with map" do
      result =
        [
          {"user-agent",
           "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/116.0"},
          {"ip-address", {127, 0, 0, 1}}
        ]
        |> Map.new()
        |> Exagon.Utils.UA.parse()

      assert %UAInspector.Result{} = result
    end
  end
end
