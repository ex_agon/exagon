defmodule ExagonWeb.Matrix.Client.AuthCase do
  import Plug.Conn
  import Phoenix.ConnTest
  use ExUnit.CaseTemplate
  import Exagon.Factory
  @endpoint ExagonWeb.Endpoint

  using do
    quote do
      alias ExagonWeb.Matrix.Client.AuthCase
      alias ExagonWeb.MatrixError
      # The default endpoint for testing
      @endpoint ExagonWeb.Endpoint

      use ExagonWeb, :verified_routes

      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import ExagonWeb.ConnCase
    end
  end

  setup tags do
    Exagon.DataCase.setup_sandbox(tags)

    user = insert(:user)
    device = insert(:device, user: user)
    account = insert(:account, user: user)
    _token = insert(:access_token, device: device)

    conn =
      Phoenix.ConnTest.build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/_matrix/client/v3/login", %{
        "type" => "m.login.password",
        "identifier" => %{"type" => "m.id.user", "user" => user.mx_user_id},
        "password" => account.password
      })

    assert {:ok, response} = Jason.decode(conn.resp_body)

    %{
      "access_token" => access_token,
      "device_id" => device_id,
      "user_id" => user_id
    } = response

    {:ok,
     %{
       conn:
         Phoenix.ConnTest.build_conn()
         |> put_req_header("authorization", <<"Bearer #{access_token}">>),
       device_id: device_id,
       user_id: user_id,
       access_token: access_token
     }}
  end
end
