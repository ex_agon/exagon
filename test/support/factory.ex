defmodule Exagon.Factory do
  use ExMachina.Ecto, repo: Exagon.Repo

  alias Exagon.Users.Schema.Device
  alias Exagon.Users.Schema.User
  alias Exagon.Users.Schema.Account
  alias Exagon.Users.Schema.AccessToken

  def user_factory() do
    %User{
      mx_user_id:
        "#{Polyjuice.Util.Identifiers.V1.UserIdentifier.generate(Exagon.Config.get_in_config(Matrix, [:server_name]))}"
    }
  end

  def device_factory() do
    %Device{mx_device_id: Device.random_device_id()}
  end

  @spec account_factory(map) :: Exagon.Users.Schema.Account.t()
  def account_factory(attrs \\ %{}) do
    password = Map.get(attrs, :password, "")

    merge_attributes(
      %Account{password: password, password_hash: Argon2.hash_pwd_salt(password)},
      attrs
    )
  end

  def access_token_factory() do
    %AccessToken{
      value: Polyjuice.Util.Randomizer.randomize(10),
      expires_at: DateTime.utc_now() |> DateTime.add(1, :minute),
      soft_logout: false
    }
  end
end
