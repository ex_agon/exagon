# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Changeset do
  import Ecto.Changeset

  @doc """
  Validate a change equals a given value

  ## Options

    * `:message` - the message on failure, defaults to "is invalid"

  ## Examples

      validate_equals(changeset, :cardinal_direction, "north")
  """
  def validate_equals(changeset, field, data, opts \\ []) do
    validate_change(changeset, field, {:equals, data}, fn _, value ->
      type = Map.fetch!(changeset.types, field)

      if Ecto.Type.equal?(type, value, data),
        do: [],
        else: [{field, {message(opts, "is invalid"), [validation: :equals, equals: data]}}]
    end)
  end

  defp message(opts, key \\ :message, default) do
    Keyword.get(opts, key, default)
  end
end
