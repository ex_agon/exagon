# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Matrix.Client.V17.OneTimeKey do
  use Ecto.Schema
  alias Exagon.Matrix.Client.V17.OneTimeKey

  @primary_key false
  @type t :: %__MODULE__{
          keys: map(),
          signatures: map()
        }
  embedded_schema do
    field :keys, :map
    field :signatures, :map
  end

  @spec from_params(params :: map()) ::
          {:ok, OneTimeKey.t()} | {:error, Ecto.Changeset.t()}
  def from_params(params) do
    params
    |> changeset()
    |> Ecto.Changeset.apply_action(:insert)
  end

  def to_map(%OneTimeKey{} = device_keys) do
    device_keys |> Map.from_struct()
  end

  defp changeset(params) do
    %OneTimeKey{}
    |> Ecto.Changeset.cast(params, [
      :keys,
      :signatures
    ])
    |> Ecto.Changeset.validate_required([:keys, :signatures],
      message: "missing required parameter"
    )
  end
end
