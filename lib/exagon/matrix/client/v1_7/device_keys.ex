# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Matrix.Client.V17.DeviceKeys do
  use Ecto.Schema
  alias Exagon.Matrix.Client.V17.DeviceKeys

  @primary_key false
  @type t :: %__MODULE__{
          algorithms: [String.t()],
          device_id: String.t(),
          keys: map(),
          signatures: map(),
          user_id: String.t()
        }
  embedded_schema do
    field :algorithms, {:array, :string}
    field :device_id, :string
    field :keys, :map
    field :signatures, :map
    field :user_id, :string
  end

  @spec from_params(
          params :: map(),
          logged_in_user_id :: String.t(),
          logged_in_device_id :: String.t()
        ) ::
          {:ok, DeviceKeys.t()} | {:error, Ecto.Changeset.t()}
  def from_params(params, logged_in_user_id, logged_in_device_id) do
    params
    |> changeset(logged_in_user_id, logged_in_device_id)
    |> Ecto.Changeset.apply_action(:insert)
  end

  def to_map(%DeviceKeys{} = device_keys) do
    device_keys |> Map.from_struct()
  end

  defp changeset(params, logged_in_user_id, logged_in_device_id) do
    %DeviceKeys{}
    |> Ecto.Changeset.cast(params, [
      :algorithms,
      :device_id,
      :keys,
      :signatures,
      :user_id
    ])
    |> Ecto.Changeset.validate_required([:algorithms, :device_id, :keys, :signatures, :user_id],
      message: "missing required parameter"
    )
    |> Exagon.Changeset.validate_equals(:device_id, logged_in_device_id)
    |> Exagon.Changeset.validate_equals(:user_id, logged_in_user_id)
  end
end
