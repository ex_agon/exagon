# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Config do
  @app_name :exagon

  def well_known() do
    matrix_conf = Application.get_env(:exagon, Matrix)
    m_homeserver = matrix_conf[:public_baseurl] || "http://#{matrix_conf[:server_name]}/"
    %{"m.homeserver" => %{"base_url" => m_homeserver}}
  end

  def get(key), do: get(key, nil)

  def get(key, default) do
    Application.get_env(@app_name, key, default)
  end

  def get_in_config(config, keys), do: Application.get_env(@app_name, config) |> get_in(keys)
end
