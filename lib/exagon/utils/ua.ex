# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Utils.UA do
  @spec parse([{String.t(), String.t()}]) :: UAInspector.Result.t() | UAInspector.Result.Bot.t()
  def parse(nil) do
    UAInspector.parse(nil)
  end

  def parse(headers) when is_list(headers) do
    hints =
      Enum.filter(headers, fn {hname, _} -> String.starts_with?(hname, "sec-ch-ua") end)
      |> UAInspector.ClientHints.new()

    case Enum.find(headers, fn {hname, _} -> hname == "user-agent" end) do
      {_, user_agent} -> UAInspector.parse(user_agent, hints)
      _ -> UAInspector.parse(nil, hints)
    end
  end

  def parse(headers) when is_map(headers) do
    headers |> Map.to_list() |> parse()
  end

  def display_name(ua) do
    case ua do
      %UAInspector.Result.Bot{name: name} -> name
      %UAInspector.Result{browser_family: browser, os_family: os} -> "#{browser} on #{os}"
    end
  end
end
