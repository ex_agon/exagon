# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Utils.Ip do
  @spec from_string!(String.t()) :: :inet.ip_address()
  def from_string!(str) do
    case from_string(str) do
      {:ok, v} -> v
      _ -> raise ArgumentError, "malformed ip address string #{str}"
    end
  end

  @doc """
  Finds an ip address in a string, returning an ok or error tuple on failure.
  """
  @spec from_string(String.t()) :: {:ok, :inet.ip_address()} | {:error, :einval}
  def from_string(str) do
    str
    |> String.to_charlist()
    |> :inet.parse_address()
  end
end
