# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Repo do
  use Ecto.Repo,
    otp_app: :exagon,
    adapter: Ecto.Adapters.Postgres

  def schema do
    quote do
      use Ecto.Schema
      @timestamps_opts type: :utc_datetime
      @primary_key {:id, Uniq.UUID, version: 7, autogenerate: true}
      @foreign_key_type Uniq.UUID

      import Ecto
      import Ecto.Changeset
      alias Exagon.Repo
    end
  end

  def repository do
    quote do
      alias Exagon.Repo
      import Ecto.Query
      alias Ecto.Changeset
    end
  end

  def finder do
    quote do
      alias Exagon.Repo
      import Ecto.Query
    end
  end

  @doc """
  When used, dispatch to the appropriate schema/repositoty/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
