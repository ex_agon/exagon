# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Finders.DeviceAndUserFromMxUserIdAndMxDeviceId do
  use Exagon.Repo, :finder
  alias Exagon.Users.Schema.User

  def find(mx_user_id, mx_device_id) do
    query =
      from(u in User,
        join: d in assoc(u, :devices),
        where: u.mx_user_id == ^mx_user_id and d.mx_device_id == ^mx_device_id,
        select: {u, d}
      )

    Repo.one(query)
  end
end
