# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Finders.UserInfoFromAccessToken do
  use Exagon.Repo, :finder
  alias Exagon.Users.Schema.AccessToken
  alias Exagon.Users.Schema.User
  alias Exagon.Users.Schema.Device
  alias Exagon.Users.Schema.Account

  @spec find(token_value :: binary()) ::
          {User.t(), Account.t(), Device.t(), AccessToken.t()} | nil
  def find(token_value) do
    case Exagon.Users.AuthTokenCache.get(token_value) do
      nil -> do_find(token_value)
      info -> info
    end
  end

  @spec do_find(token_value :: binary()) ::
          {User.t(), Account.t(), Device.t(), AccessToken.t()} | nil
  defp do_find(token_value) do
    query =
      from(at in AccessToken,
        join: d in assoc(at, :device),
        join: u in assoc(d, :user),
        join: a in assoc(u, :account),
        where: at.value == ^token_value,
        select: {u, a, d, at}
      )

    result = Repo.one(query)

    if not is_nil(result) do
      Exagon.Users.AuthTokenCache.put(token_value, result)
    end

    result
  end
end
