# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Finders.UserFromMxUserId do
  use Exagon.Repo, :finder
  alias Exagon.Users.Schema.User
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier

  @spec find(mx_user_id :: binary()) :: User | nil
  def find(mx_user_id) when is_binary(mx_user_id) do
    mx_user_id |> user_by_mx_user_id_query |> Repo.one()
  end

  @spec find(mx_user_id :: UserIdentifier.t()) :: User | nil
  def find(%UserIdentifier{} = user_identifier) do
    find("#{user_identifier}")
  end

  @spec user_by_mx_user_id_query(mx_user_id :: String.t()) :: Ecto.Queryable.t()
  defp user_by_mx_user_id_query(mx_user_id) when is_binary(mx_user_id) do
    from u in User, where: u.mx_user_id == ^mx_user_id
  end
end
