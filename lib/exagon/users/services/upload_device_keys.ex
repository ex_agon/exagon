# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Services.UploadDeviceKeys do
  require Logger

  alias Exagon.Matrix.Client.V17.DeviceKeys

  @spec call(device_keys :: DeviceKeys.t(), mx_user_id :: String.t(), mx_device_id :: String.t()) ::
          {:ok, any}
          | {:error, :invalid_signature}
          | {:error, :invalid}
          | {:error, :key_not_found}
  def call(%DeviceKeys{} = device_keys, mx_user_id, mx_device_id) do
    with {_user, device} <-
           Exagon.Users.find_device_and_user_from_mx_user_id_and_mx_device_id(
             mx_user_id,
             mx_device_id
           ),
         {:ok, verify_key} <- get_device_key(device_keys, mx_device_id),
         {:signed, true} <-
           {:signed,
            Polyjuice.Util.JSON.signed?(
              DeviceKeys.to_map(device_keys),
              mx_user_id,
              Polyjuice.Util.Ed25519.VerifyKey.from_base64(verify_key, mx_device_id)
            )},
         {:ok, device_key} <-
           Exagon.Users.Repositories.DeviceKeyRepository.create_device_key(device, %{
             key_json: DeviceKeys.to_map(device_keys)
           }) do
      {:ok, device_key}
    else
      {:signed, false} ->
        {:error, :invalid_signature}

      {:error, changeset} ->
        Logger.error("Unexpected error: #{inspect(changeset)}")
        {:error, :unexpected}
    end
  end

  defp get_device_key(%DeviceKeys{} = device_key, device_id) do
    key = device_key |> Map.get(:keys) |> Map.get("ed25519:#{device_id}")

    case key do
      nil -> {:error, :key_not_found}
      key -> {:ok, key}
    end
  end
end
