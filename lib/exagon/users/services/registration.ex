# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Services.Registration do
  require Logger
  alias Exagon.Users.Repositories.AccessTokenRepository
  alias Exagon.Users.Repositories.UserRepository
  alias Exagon.Users.Repositories.AccountRepository
  alias Exagon.Users.Repositories.DeviceRepository

  @spec call(kind :: String.t(), map(), map()) ::
          {:ok, map()}
          | {:error, :m_invalid_username}
          | {:error, :m_user_in_use}
          | {:error, :unexpected}

  def call(
        kind,
        %{
          device_id: device_id,
          initial_device_display_name: initial_device_display_name,
          password: password,
          refresh_token: _refresh_token,
          username: username
        },
        device_info
      )
      when kind in ["user", "guest"] do
    with server_name when not is_nil(server_name) <-
           Exagon.Config.get_in_config(Matrix, [:server_name]),
         access_token_lifetime when not is_nil(access_token_lifetime) <-
           Exagon.Config.get_in_config(Matrix, [:access_token, :lifetime]),
         {:ok, user_identifier} <-
           Polyjuice.Util.Identifiers.V1.UserIdentifier.new(username, server_name),
         {:ok, %{user: user, device: device, account: account, access_token: access_token}} <-
           create_user(
             kind,
             user_identifier,
             device_id,
             initial_device_display_name ||
               Exagon.Utils.UA.parse(device_info) |> Exagon.Utils.UA.display_name(),
             password,
             device_info,
             access_token_lifetime
           ) do
      # Do stuff on user registration, like publishing the info
      {:ok, %{user: user, device: device, account: account, access_token: access_token}}
    else
      {:error, :invalid_user_id} ->
        {:error, :m_invalid_username}

      {:error, :user, changeset, _} ->
        case changeset |> first_error(:mx_user_id) do
          "mx_user_id_is_required" -> {:error, :m_invalid_username}
          "mx_user_id_already_exists" -> {:error, :m_user_in_use}
          "invalid_mx_user_id" -> {:error, :m_invalid_username}
        end

      {:error, _, changeset, _} ->
        # Changeset errors from :device, :account, :access_token should have been handled before
        Logger.error("Unexpected error: #{inspect(changeset)}")

      error ->
        Logger.error("unmatched error: #{inspect(error)}")
        {:error, :unexpected}
    end
  end

  defp create_user(
         kind,
         user_identifier,
         device_id,
         initial_device_display_name,
         password,
         device_info,
         access_token_lifetime
       ) do
    Ecto.Multi.new()
    |> UserRepository.create_user_multi(:user, %{mx_user_id: "#{user_identifier}"})
    |> AccountRepository.create_account_multi(:account, %{password: password, kind: kind})
    |> DeviceRepository.create_device_multi(:device, %{
      mx_device_id: device_id,
      display_name: initial_device_display_name,
      device_info: device_info
    })
    |> AccessTokenRepository.create_access_token_multi(:access_token, %{
      expires_at: DateTime.add(DateTime.utc_now(), access_token_lifetime, :second)
    })
    |> Exagon.Repo.transaction()
  end

  defp first_error(changeset, field) do
    case changeset
         |> Ecto.Changeset.traverse_errors(fn {msg, _} -> msg end)
         |> Map.get(field) do
      [] -> nil
      [h | _] -> h
    end
  end
end
