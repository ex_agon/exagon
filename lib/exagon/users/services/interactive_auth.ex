# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Services.InteractiveAuth do
  @moduledoc """
  Interactive auth service.

  Interactive auth service manages [user-interactive authentication API](https://spec.matrix.org/v1.7/client-server-api/#user-interactive-authentication-api) call such as:
   - returning new auth flow sessions to client on first endpoint call
   - on later calls, retreive auth session and manage authentication stages
   - let the user pass when some stage complete
   - return error on unexpected behaviours

  Sessions are stored in [Exagon.Users.InteractiveAuthSessionCache] cache.
  """
  require Logger
  alias Polyjuice.Util.ClientAPIErrors
  alias Exagon.Users.InteractiveAuthSessionCache

  @spec call(endpoint :: String.t(), body :: map()) ::
          {:ok, map()} | {:ok, :completed} | {:error, integer(), map()}
  def call(_endpoint, %{"auth" => auth} = _request) when not is_nil(auth) do
    with {:auth, %{"session" => session, "type" => type}} <- {:auth, auth},
         {:flow, {:ok, flow}} when not is_nil(flow) <- {:flow, get_session(session)} do
      case authenticate(type, auth, flow) do
        {:ok} ->
          flow = put_in(flow["completed"], [type] ++ Map.get(flow, "completed", []))

          case has_completed_stage?(flow) do
            true ->
              delete_session(flow)
              {:ok, :completed}

            false ->
              update_session(flow)
              {:error, 401, flow}
          end

        {:ko, errcode, error} ->
          {:error, 401, Map.merge(flow, %{"errcode" => errcode, "error" => error})}
      end
    else
      {:auth, _} ->
        # "session" or "type" parameter is missing from "auth" paramters
        {:error, 400, ClientAPIErrors.MBadJson.new("Invalid auth parameter")}

      {:flow, _} ->
        # "session" not found
        {:error, 400, ClientAPIErrors.MBadJson.new("Session not found or expired")}
    end
  end

  def call(endpoint, _) do
    case create_session(endpoint) do
      {:ok, flow} ->
        {:ok, flow}

      {:error, :no_flow_defined_for_endpoint} ->
        {:error, 400,
         ExagonWeb.Matrix.V17.Client.Errors.ExInternalError.new(
           "No interactive flow defined for endpoint #{endpoint}"
         )}
    end
  end

  defp has_completed_stage?(flow) do
    # Traverse all flows and find if one exists with all completed stages
    Enum.map(flow["flows"], fn f -> f["stages"] -- flow["completed"] end)
    |> Enum.find(fn elem -> length(elem) == 0 end) != nil
  end

  @spec create_session(endpoint :: String.t()) ::
          {:ok, map()} | {:error, :no_flow_defined_for_endpoint}
  defp create_session(endpoint) do
    with flows when not is_nil(flows) <- Application.get_env(:exagon, Matrix)[:interactive_auth],
         flow when not is_nil(flow) <- Map.get(flows, endpoint) do
      session_id =
        Polyjuice.Util.Randomizer.unique_id()

      flow = put_in(flow["session"], session_id)
      InteractiveAuthSessionCache.put(session_id, flow)
      {:ok, flow}
    else
      nil ->
        Logger.warning("No interactive flow defined for endpoint #{endpoint}")
        {:error, :no_flow_defined_for_endpoint}
    end
  end

  @spec get_session(session_id :: String.t()) :: {:ok, map()} | {:ok, nil}
  defp get_session(session_id) do
    {:ok, InteractiveAuthSessionCache.get(session_id)}
  end

  defp update_session(flow) do
    case Map.get(flow, "session") do
      nil ->
        :ok

      session_id ->
        InteractiveAuthSessionCache.put(session_id, flow)
        :ok
    end
  end

  defp delete_session(flow) do
    case Map.get(flow, "session") do
      nil ->
        :ok

      session_id ->
        InteractiveAuthSessionCache.delete(session_id)
        :ok
    end
  end

  defp authenticate("m.login.dummy", _auth, _session), do: {:ok}

  defp authenticate(type, _auth, _session),
    do: {:ko, "M_FORBIDDEN", "Unexpected authentication type #{type}"}
end
