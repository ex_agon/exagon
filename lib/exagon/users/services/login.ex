# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Services.Login do
  require Logger
  alias Ecto.Changeset
  alias Exagon.Users
  alias Exagon.Users.Repositories.DeviceRepository
  alias Exagon.Users.Repositories.AccessTokenRepository

  def call(
        %{
          device_id: device_id,
          identifier: identifier,
          initial_device_display_name: initial_device_display_name,
          password: password,
          refresh_token: _refresh_token,
          token: _token,
          type: type
        },
        device_info
      )
      when type == "m.login.password" do
    with {:identifier, %{"type" => "m.id.user", "user" => username}} <- {:identifier, identifier},
         server_name when not is_nil(server_name) <-
           Exagon.Config.get_in_config(Matrix, [:server_name]),
         access_token_lifetime when not is_nil(access_token_lifetime) <-
           Exagon.Config.get_in_config(Matrix, [:access_token, :lifetime]),
         {:ok, user_id} <- Exagon.Users.parse_username(username, server_name),
         {:user_info, {user, account}} <-
           {:user_info, Exagon.Users.find_user_account_from_mx_user_id("#{user_id}")},
         {:verify_pass, true} <-
           {:verify_pass, Argon2.verify_pass(password, account.password_hash)},
         {:ok, %{user: _, device: device, access_token: access_token}} <-
           get_or_create_device_with_access_token(
             user,
             device_id,
             initial_device_display_name ||
               Exagon.Utils.UA.parse(device_info) |> Exagon.Utils.UA.display_name(),
             device_info,
             access_token_lifetime
           ) do
      {:ok, {user, account, device, access_token}}
    else
      {:identifier, _} ->
        {:error, :invalid_identifier}

      {:error, :invalid_username} ->
        Argon2.no_user_verify()
        {:error, :invalid_username}

      {:user_info, nil} ->
        Argon2.no_user_verify()
        {:ok, :forbidden}

      {:verify_pass, false} ->
        {:ok, :forbidden}

      {:error, _, changeset, _} ->
        # Changeset errors from get_or_create_device should have been handled before
        Logger.error("Unexpected error: #{inspect(changeset)}")
        {:error, :unexpected}

      error ->
        Logger.error("unmatched error: #{inspect(error)}")
        {:error, :unexpected}
    end
  end

  def call(_), do: {:error, :unsupported_login_type}

  @spec get_or_create_device_with_access_token(
          user :: User.t(),
          mx_device_id :: String.t(),
          display_name :: String.t(),
          device_info :: String.t(),
          access_token_lifetime :: integer()
        ) :: {:ok, map} | {:error, Changeset.t()}
  defp get_or_create_device_with_access_token(
         user,
         mx_device_id,
         display_name,
         device_info,
         access_token_lifetime
       ) do
    case Users.find_device_from_user_and_mx_device_id(user, mx_device_id) do
      nil ->
        Ecto.Multi.new()
        |> Ecto.Multi.put(:user, user)
        |> DeviceRepository.create_device_multi(:device, %{
          mx_device_id: mx_device_id,
          display_name: display_name,
          device_info: device_info
        })
        |> AccessTokenRepository.create_access_token_multi(:access_token, %{
          expires_at: DateTime.add(DateTime.utc_now(), access_token_lifetime, :second)
        })
        |> Exagon.Repo.transaction()

      device ->
        Ecto.Multi.new()
        |> Ecto.Multi.put(:user, user)
        |> Ecto.Multi.put(:device, device)
        |> AccessTokenRepository.create_access_token_multi(:access_token, %{
          expires_at: DateTime.add(DateTime.utc_now(), access_token_lifetime, :second)
        })
        |> Exagon.Repo.transaction()
    end
  end
end
