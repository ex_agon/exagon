# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Schema.User do
  @moduledoc """
  `User` represents and stores data from any matrix user:
   - `mx_user_id` represents the unique matrix User ID like `@localpart:domain`.
   - `display_name` stores the display name chosen by the user.
   - `devices`, the list of known matrix Devices associated with this user.

  `User`s who belongs to the local Exagon homeserver  are linked to a `Exagon.Users.Schema.Account` which store instance's specific data
  like password.
  """
  use Exagon.Repo, :schema
  alias Exagon.Users.Schema.Account
  alias Exagon.Users.Schema.Device

  @type t :: %__MODULE__{
          mx_user_id: String.t(),
          display_name: String.t(),
          avatar_url: String.t(),
          account: String.t(),
          devices: [Device.t()]
        }

  schema "users" do
    field(:mx_user_id, :string)
    field(:display_name, :string)
    field(:avatar_url, :string)
    has_one(:account, Account)
    has_many(:devices, Device)
    timestamps()
  end

  @doc """
  Create user changeset using a map of attributes.

  On error, changeset error message contains the following codes:
   - field `mx_user_id`:
     - `mx_user_id_is_required` if `mx_user_id` is missing
     - `mx_user_id_already_exists` when the given `mx_user_id` is already used in database by another user
     - `invalid_mx_user_id` when the given `mx_user_id` doesn't respect the [user ID grammar](https://spec.matrix.org/v1.7/appendices/#user-identifiers)
  """
  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:mx_user_id, :display_name, :avatar_url])
    |> validate_required([:mx_user_id], message: "mx_user_id_is_required")
    |> validate_mx_user_id()
    |> unique_constraint(:mx_user_id, message: "mx_user_id_already_exists")
  end

  # Validate user_id format
  defp validate_mx_user_id(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{mx_user_id: user_id}} ->
        if Polyjuice.Util.Identifiers.V1.UserIdentifier.valid?(user_id) do
          changeset
        else
          Ecto.Changeset.add_error(changeset, :mx_user_id, "invalid_mx_user_id")
        end

      _ ->
        changeset
    end
  end

  defp validate_mx_user_id(changeset), do: changeset
end
