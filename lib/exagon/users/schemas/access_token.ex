# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Schema.AccessToken do
  use Exagon.Repo, :schema
  alias Exagon.Users.Schema.Device

  @type t :: %__MODULE__{
          value: String.t(),
          device: Device.t(),
          last_seen: DateTime.t(),
          soft_logout: boolean(),
          expires_at: DateTime.t()
        }

  schema "access_tokens" do
    field(:value, :string)
    belongs_to(:device, Device)
    field(:last_seen, :utc_datetime)
    field(:soft_logout, :boolean)
    field(:expires_at, :utc_datetime)
    field(:refresh_token, :string)
    timestamps()
  end

  def changeset(token, attrs \\ %{}) do
    token
    |> cast(attrs, [:value, :last_seen, :soft_logout, :expires_at, :refresh_token])
    |> assoc_constraint(:device)
    |> generate_missing_value()
    |> generate_missing_refresh_token()
  end

  def update_changeset(token, attrs \\ %{}) do
    changeset(token, attrs)
  end

  defp generate_missing_value(changeset) do
    case fetch_field(changeset, :value) do
      {:data, nil} -> change(changeset, value: generate())
      _ -> changeset
    end
  end

  defp generate_missing_refresh_token(changeset) do
    case fetch_field(changeset, :refresh_token) do
      {:data, nil} -> change(changeset, refresh_token: generate())
      _ -> changeset
    end
  end

  def generate() do
    Polyjuice.Util.Randomizer.unique_id()
  end
end
