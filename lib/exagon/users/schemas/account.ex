# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Schema.Account do
  use Exagon.Repo, :schema
  alias Exagon.Users.Schema.User
  alias Exagon.Users.Schema.AccountData

  @type t :: %__MODULE__{
          user: User.t(),
          password: String.t(),
          password_hash: String.t(),
          kind: String.t(),
          activated: boolean()
          # filters: [Filter.t()]
        }

  schema "accounts" do
    belongs_to(:user, User)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    field(:kind, :string)
    field(:activated, :boolean)
    has_many(:account_data, AccountData)
    # has_many :filters, Filter
    timestamps()
  end

  def changeset(account, attrs \\ %{}) do
    account
    |> cast(attrs, [:kind, :password, :activated])
    |> validate_inclusion(:kind, ["user", "guest"], message: "invalid_account_kind")
    |> assoc_constraint(:user)
    |> put_pass_hash()
  end

  defp put_pass_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Argon2.add_hash(password))
  end

  defp put_pass_hash(changeset), do: changeset
end
