# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Schema.Device do
  @moduledoc """
  `Device` stores information abouth matrix users' devices:
   - `mx_device_id` represents the unique matrix Device ID.
   - `display_name` stores the display name chosen by the user.
   - `user` the matrix user the device belongs to.
   - `device_keys`, `one_time_device_keys`, devices keys uploaded from the client
   - `access_tokens`, [access tokens](https://spec.matrix.org/v1.7/client-server-api/#using-access-tokens) used for client authentication.
   - `device_info`, map of information about the device: contains client IP address and several HTTP headers sent by the client
   like `user-agent`, and `sec-ch-ua*`.
  """
  use Exagon.Repo, :schema
  alias Exagon.Users.Schema.User
  alias Exagon.Users.Schema.DeviceKey
  alias Exagon.Users.Schema.AccessToken
  alias Exagon.Users.Schema.OneTimeDeviceKey

  @type t :: %__MODULE__{
          mx_device_id: String.t(),
          display_name: String.t(),
          user: User.t(),
          device_keys: [DeviceKey.t()],
          one_time_device_keys: [OneTimeDeviceKey.t()],
          access_tokens: [AccessToken.t()],
          device_info: map()
        }

  schema "devices" do
    field(:mx_device_id, :string)
    field(:display_name, :string)
    belongs_to(:user, User)
    has_many(:device_keys, DeviceKey)
    has_many(:one_time_device_keys, OneTimeDeviceKey)
    has_many(:access_tokens, AccessToken)
    field(:device_info, :map)
    timestamps()
  end

  @doc """
  Create device changeset using a map of attributes.
  """
  def changeset(device, attrs \\ %{}) do
    device
    |> cast(attrs, [:mx_device_id, :display_name, :device_info, :user_id])
    |> put_mx_device_id()
    |> assoc_constraint(:user)
  end

  defp put_mx_device_id(%Ecto.Changeset{valid?: true, changes: changes} = changeset) do
    case changes do
      %{mx_device_id: mx_device_id} when not is_nil(mx_device_id) -> changeset
      _ -> put_change(changeset, :mx_device_id, random_device_id())
    end
  end

  @doc """
  Generates a unique ID usable for uniquelly identifying a device
  """
  @spec random_device_id :: String.t()
  def random_device_id() do
    Polyjuice.Util.Identifiers.DeviceIdentifier.generate()
  end
end
