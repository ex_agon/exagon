# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users do
  alias Exagon.Users.Schema.AccessToken
  alias Exagon.Users.Schema.User
  alias Exagon.Users.Schema.Device
  alias Exagon.Users.Schema.Account
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier

  @spec find_user_info_from_access_token(token_value :: binary()) ::
          {User.t(), Account.t(), Device.t(), AccessToken.t()} | nil
  def find_user_info_from_access_token(token_value) do
    Exagon.Users.Finders.UserInfoFromAccessToken.find(token_value)
  end

  @spec find_user_account_from_mx_user_id(mx_user_id :: String.t()) ::
          {User.t(), Account.t()} | nil
  def find_user_account_from_mx_user_id(mx_user_id) do
    Exagon.Users.Finders.UserAccountFromMxUserId.find(mx_user_id)
  end

  @spec find_device_from_user_and_mx_device_id(user :: User.t(), mx_device_id :: String.t()) ::
          Device | nil
  def find_device_from_user_and_mx_device_id(user, mx_device_id) do
    Exagon.Users.Finders.DeviceFromUserAndMxDeviceId.find(user, mx_device_id)
  end

  @spec find_device_and_user_from_mx_user_id_and_mx_device_id(
          mx_user_id :: String.t(),
          mx_device_id :: String.t()
        ) ::
          {User, Device} | nil
  def find_device_and_user_from_mx_user_id_and_mx_device_id(mx_user_id, mx_device_id) do
    Exagon.Users.Finders.DeviceAndUserFromMxUserIdAndMxDeviceId.find(mx_user_id, mx_device_id)
  end

  @spec interactive_auth(endpoint :: String.t(), client_request :: map()) ::
          {:ok, map()} | {:ok, :completed} | {:error, integer(), map()}
  def interactive_auth(endpoint, client_request) do
    Exagon.Users.Services.InteractiveAuth.call(endpoint, client_request)
  end

  @spec register(kind :: String.t(), params :: map(), device_info :: map()) ::
          {:ok, map()}
          | {:error, :m_invalid_username}
          | {:error, :m_user_in_use}
          | {:error, :unexpected}
  def register(kind, params, device_info) do
    Exagon.Users.Services.Registration.call(kind, params, device_info)
  end

  @spec login(params :: map(), device_info :: String.t()) ::
          {:ok, {User.t(), Account.t(), Device.t(), AccessToken.t()}}
          | {:error, :invalid_identifier}
          | {:error, :invalid_username}
          | {:ok, :forbidden}
  def login(params, device_info) do
    Exagon.Users.Services.Login.call(params, device_info)
  end

  @doc """
  See `Exagon.Users.Services.UploadDeviceKeys.call/3`
  """
  def upload_device_keys(device_key, mx_user_id, mx_device_id) do
    Exagon.Users.Services.UploadDeviceKeys.call(device_key, mx_user_id, mx_device_id)
  end

  @spec parse_username(username :: String.t(), server_name :: String.t()) ::
          {:ok, Polyjuice.Util.Identifiers.V1.UserIdentifier.t()} | {:error, :invalid_username}
  @doc """
  Parse a given user name.
  `username` is checked both as localpart with `server_name` or as fully qualified.
  """
  def parse_username(username, server_name) do
    with {:error, _} <- UserIdentifier.parse(username),
         {:error, _} <- UserIdentifier.new(username, server_name) do
      {:error, :invalid_username}
    else
      {:ok, user} -> {:ok, user}
    end
  end

  @spec is_username_available(username :: String.t()) ::
          {:error, :invalid_username} | {:ok, boolean}
  def is_username_available(username) do
    with server_name when not is_nil(server_name) <-
           Exagon.Config.get_in_config(Matrix, [:server_name]),
         {:ok, user_id} <- parse_username(username, server_name),
         nil <- Exagon.Users.find_user_account_from_mx_user_id("#{user_id}") do
      {:ok, true}
    else
      {:error, :invalid_username} -> {:error, :invalid_username}
      {_, _} -> {:ok, false}
    end
  end
end
