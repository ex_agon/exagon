# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Repositories.AccessTokenRepository do
  use Exagon.Repo, :repository
  alias Exagon.Users.Schema.AccessToken
  alias Exagon.Users.Schema.Device

  @spec create_access_token(device :: Device.t(), map()) ::
          {:ok, AccessToken} | {:error, Changeset.t()}
  def create_access_token(device, attrs \\ %{}) do
    create_changeset(device, attrs) |> Repo.insert()
  end

  @spec create_access_token_multi(Ecto.Multi.t(), atom(), any) :: Ecto.Multi.t()
  def create_access_token_multi(multi, name, attrs \\ %{}) do
    multi
    |> Ecto.Multi.insert(name, fn %{device: device} -> create_changeset(device, attrs) end)
  end

  defp create_changeset(device, attrs),
    do: Ecto.build_assoc(device, :access_tokens) |> AccessToken.changeset(attrs)

  @spec update_access_token(token :: AccessToken.t(), map()) ::
          {:ok, AccessToken} | {:error, Changeset.t()}
  def update_access_token(token, attrs) do
    token |> AccessToken.changeset(attrs) |> Repo.update()
  end

  @spec delete_access_token(token :: AccessToken.t()) ::
          {:ok, AccessToken} | {:error, Changeset.t()}
  def delete_access_token(token) do
    Repo.delete(token)
  end

  @spec get_by_id(token_id :: binary()) :: AccessToken | nil
  def get_by_id(token_id) do
    Repo.get(AccessToken, token_id)
  end
end
