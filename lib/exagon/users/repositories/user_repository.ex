# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Repositories.UserRepository do
  use Exagon.Repo, :repository
  alias Exagon.Users.Schema.User

  @spec create_user(attrs :: map()) ::
          {:ok, User} | {:error, Changeset.t()}
  def create_user(attrs \\ %{}) do
    create_changeset(attrs) |> Repo.insert()
  end

  def create_user_multi(multi, name, attrs) do
    multi
    |> Ecto.Multi.insert(name, create_changeset(attrs))
  end

  defp create_changeset(attrs), do: User.changeset(%User{}, attrs)

  @spec update_user(user :: User.t(), map()) ::
          {:ok, User} | {:error, Changeset.t()}
  def update_user(user, attrs) do
    user |> User.changeset(attrs) |> Repo.update()
  end

  @spec delete_user(user :: User.t()) ::
          {:ok, User} | {:error, Changeset.t()}
  def delete_user(user) do
    Repo.delete(user)
  end

  @spec get_by_id(user_id :: binary()) :: User | nil
  def get_by_id(user_id) do
    Repo.get(User, user_id)
  end
end
