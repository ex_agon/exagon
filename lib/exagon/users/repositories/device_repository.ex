# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Exagon.Users.Repositories.DeviceRepository do
  use Exagon.Repo, :repository
  alias Exagon.Users.Schema.Device

  def create_device(user, attrs \\ %{}) do
    create_changeset(user, attrs) |> Repo.insert()
  end

  @spec create_device_multi(Ecto.Multi.t(), any, any) :: Ecto.Multi.t()
  def create_device_multi(multi, name, attrs) do
    multi
    |> Ecto.Multi.insert(name, fn %{user: user} -> create_changeset(user, attrs) end)
  end

  defp create_changeset(user, attrs),
    do: Ecto.build_assoc(user, :devices) |> Device.changeset(attrs)
end
