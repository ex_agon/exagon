# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Router do
  use ExagonWeb, :router

  pipeline :api do
    plug ExagonWeb.Matrix.Client.DeviceInfoPlug
  end

  pipeline :client_api do
    plug :cors

    plug Plug.Parsers,
      parsers: [ExgonWeb.JSONParser],
      pass: ["*/*"],
      json_decoder: Phoenix.json_library()
  end

  scope "/_matrix/client/", ExagonWeb.Matrix.Client do
    get "/versions", ClientController, :versions
  end

  scope "/_matrix/client/r0", ExagonWeb.Matrix.Client.R0 do
    pipe_through [:api, :client_api]
    forward "/", ClientRouter
  end

  scope "/_matrix/client/v3", ExagonWeb.Matrix.Client.V17 do
    pipe_through [:api, :client_api]
    forward "/", ClientRouter
  end

  scope "/.well-known", ExagonWeb.Matrix.Client do
    pipe_through :client_api
    get "/matrix/client", WellKnownController, :server_discovery
  end

  def cors(conn, _) do
    conn =
      conn
      |> put_resp_header("access-control-allow-origin", "*")
      |> put_resp_header("access-control-allow-methods", "GET, POST, PUT, DELETE, OPTIONS")
      |> put_resp_header(
        "access-control-allow-headers",
        "X-Requested-With, Content-Type, Authorization"
      )

    case conn.method do
      "OPTIONS" ->
        send_resp(conn, 204, "")
        |> halt()

      _ ->
        conn
    end
  end
end
