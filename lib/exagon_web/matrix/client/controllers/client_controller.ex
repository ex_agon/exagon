# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.ClientController do
  use ExagonWeb, :controller

  # def cors(conn, _opts) do
  #  conn
  #  |> put_resp_header("access-control-allow-origin", "*")
  #  |> put_resp_header("access-control-allow-methods", "GET, POST, PUT, DELETE, OPTIONS")
  #  |> put_resp_header(
  #    "access-control-allow-headers",
  #    "X-Requested-With, Content-Type, Authorization"
  #  )
  #  |> send_resp(204, "")
  # end

  def versions(conn, _) do
    versions = %{
      "versions" => ["v1.0", "v1.1", "v1.2", "v1.3", "v1.4", "v1.5", "v1.6", "v1.7"]
    }

    json(conn, versions)
  end
end
