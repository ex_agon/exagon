# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.R0.Controllers.RegisterController do
  use ExagonWeb, :controller
  alias Polyjuice.Util.ClientAPIErrors

  @register_default_kind "user"

  def available(%{params: %{"username" => username}} = conn, _opts) do
    case Exagon.Users.is_username_available(username) do
      {:ok, true} ->
        conn |> json(%{"available" => true})

      {:ok, false} ->
        conn
        |> put_status(400)
        |> json(Polyjuice.Util.ClientAPIErrors.MUserInUse.new())
        |> halt()

      _ ->
        conn
        |> put_status(400)
        |> json(Polyjuice.Util.ClientAPIErrors.MInvalidUsername.new())
        |> halt()
    end
  end

  def available(conn, _opts) do
    conn |> json(ClientAPIErrors.MBadJson.new()) |> halt
  end

  def register(conn, _opts) do
    with :ok <- validate_registration_enabled(conn),
         {:ok, kind} <- validate_kind(conn),
         {:ok, params} <- register_params(conn),
         device_info <- conn.assigns[:device_info],
         {:ok, %{user: user, device: device, account: _account, access_token: access_token}} <-
           Exagon.Users.register(kind, params, device_info) do
      response =
        case params.inhibit_login do
          false ->
            %{
              access_token: access_token.value,
              device_id: device.mx_device_id,
              expires_in_ms:
                DateTime.diff(access_token.expires_at, DateTime.utc_now(), :millisecond),
              refresh_token: access_token.refresh_token,
              user_id: user.mx_user_id
            }

          true ->
            %{user_id: user.mx_user_id}
        end

      conn |> json(response)
    else
      {:error, :m_invalid_username} ->
        conn
        |> put_status(400)
        |> json(ClientAPIErrors.MInvalidUsername.new())
        |> halt()

      {:error, :m_user_in_use} ->
        conn
        |> put_status(400)
        |> json(ClientAPIErrors.MUserInUse.new())
        |> halt()

      {:error, :unexpected} ->
        ExagonWeb.Matrix.V17.Client.Errors.ExInternalError.new()

      {:error, %ClientAPIErrors.MForbidden{} = error} ->
        conn
        |> put_status(403)
        |> json(error)
        |> halt()

      {:error, error} ->
        conn
        |> put_status(400)
        |> json(error)
        |> halt()
    end
  end

  defp validate_registration_enabled(_conn) do
    case Exagon.Config.get_in_config(Matrix, [:registration, :enabled]) do
      true -> :ok
      _ -> {:error, ClientAPIErrors.MForbidden.new("Registration disabled")}
    end
  end

  defp validate_kind(%{params: params}) do
    case params["kind"] do
      kind when kind in ["user", "guest"] -> {:ok, kind}
      nil -> {:ok, @register_default_kind}
      _ -> {:error, ClientAPIErrors.MBadJson.new("Invalid kind value")}
    end
  end

  defmodule RegisterRequest do
    use Ecto.Schema
    @register_default_inhibit_login false
    @type t :: %__MODULE__{
            device_id: String.t(),
            inhibit_login: boolean(),
            initial_device_display_name: String.t(),
            password: String.t(),
            refresh_token: boolean(),
            username: String.t()
          }

    embedded_schema do
      field :device_id, :string
      field :inhibit_login, :boolean, default: @register_default_inhibit_login
      field :initial_device_display_name, :string
      field :password, :string
      field :refresh_token, :boolean
      field :username, :string
    end

    @spec from_params(params :: map()) ::
            {:ok, RegisterRequest.t()} | {:error, Ecto.Changeset.t()}
    def from_params(params) do
      params |> changeset() |> Ecto.Changeset.apply_action(:insert)
    end

    def changeset(params) do
      %RegisterRequest{}
      |> Ecto.Changeset.cast(params, [
        :device_id,
        :inhibit_login,
        :initial_device_display_name,
        :password,
        :refresh_token,
        :username
      ])
      |> ExagonWeb.Matrix.V17.Client.RequestUtils.generate_device_id()
      |> ExagonWeb.Matrix.V17.Client.RequestUtils.generate_username()
    end
  end

  defp register_params(%{params: params}) do
    case RegisterRequest.from_params(params) do
      {:ok, request} ->
        {:ok, request}

      {:error, changeset} ->
        {:error, ClientAPIErrors.MBadJson.new("Invalid request parameter: #{inspect(changeset)}")}
    end
  end
end
