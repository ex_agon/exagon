# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.InteractiveAuthPlug do
  use ExagonWeb, :controller
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    endpoint = get_endpoint(conn)

    case Exagon.Users.interactive_auth(endpoint, conn.body_params) do
      {:ok, :completed} ->
        conn

      {:ok, flow} ->
        conn |> put_status(:unauthorized) |> json(flow) |> halt()

      {:error, status, response} ->
        conn
        |> put_status(status)
        |> json(response)
        |> halt()
    end
  end

  defp get_endpoint(conn) do
    conn.method <> " " <> conn.request_path
  end
end
