# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.DeviceInfoPlug do
  @moduledoc """
  Extracts request headers useful for UA identification and add them as a map to `conn.assigns[:device_info]`.
  Example device_info map content:
  ```
  %{
  "ip-address" => {127, 0, 0, 1},
  "user-agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/116.0"
  }
  ```
  """
  use ExagonWeb, :controller
  import Plug.Conn

  @assign_key :device_info

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    device_info =
      (Enum.filter(conn.req_headers, fn {hname, _} ->
         String.starts_with?(hname, "sec-ch-ua") || hname == "user-agent"
       end) ++ [{"ip-address", conn.remote_ip |> :inet_parse.ntoa() |> to_string()}])
      |> Map.new()

    put_in(conn.assigns[@assign_key], device_info)
  end
end
