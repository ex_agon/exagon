# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.RequireAuthPlug do
  use ExagonWeb, :controller
  import Plug.Conn
  alias Polyjuice.Util.ClientAPIErrors

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    fetch_access_token(conn) |> find_user_info() |> validate_access_token()
  end

  defp fetch_access_token(conn) do
    token =
      case get_req_header(conn, "authorization") do
        [<<"Bearer ", token::binary>>] ->
          String.trim(token)

        _ ->
          Map.get(conn.query_params, "access_token")
      end

    case token do
      nil ->
        raise ExagonWeb.MatrixError,
          message: "Auth token not found",
          matrix_error: ClientAPIErrors.MMissingToken,
          plug_status: 403

      _ ->
        put_in(conn.assigns[:access_token_value], token)
    end
  end

  defp find_user_info(conn) do
    with token_value when not is_nil(token_value) <- conn.assigns[:access_token_value],
         {user, account, device, access_token} <-
           Exagon.Users.find_user_info_from_access_token(token_value) do
      put_in(conn.assigns[:user_info], %{
        user: user,
        account: account,
        device: device,
        access_token: access_token
      })
    else
      _ ->
        # the access token was never valid.
        raise ExagonWeb.MatrixError,
          message: "Unknown token",
          matrix_error: ClientAPIErrors.MUnknownToken,
          plug_status: 403
    end
  end

  defp validate_access_token(conn) do
    with %{access_token: access_token} <- conn.assigns[:user_info] do
      if access_token.soft_logout == true ||
           !DateTime.after?(access_token.expires_at, DateTime.utc_now()) do
        raise ExagonWeb.MatrixError,
          message: "Soft logout",
          matrix_error: ClientAPIErrors.MUnknownToken,
          plug_status: 403,
          additional_fields: %{soft_logout: true}
      else
        conn
      end
    else
      _ -> conn
    end
  end
end
