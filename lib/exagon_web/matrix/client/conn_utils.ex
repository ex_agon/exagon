# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.ConnUtils do
  alias Exagon.Users.Schema.User
  alias Exagon.Users.Schema.Device

  def get_user_id(conn) do
    case get_in(conn.assigns, [:user_info, :user]) do
      nil -> nil
      %User{} = user -> user.mx_user_id
    end
  end

  def get_device_id(conn) do
    case get_in(conn.assigns, [:user_info, :device]) do
      nil -> nil
      %Device{} = device -> device.mx_device_id
    end
  end
end
