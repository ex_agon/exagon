# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.V17.Client.RequestUtils do
  use Ecto.Schema
  alias Ecto.Changeset

  @doc """
  Except a changeset to contain data coming from a client request. If it contains a `device_id` field, a new change is added with a generated deviceID.
  """
  def generate_device_id(changeset) do
    case Changeset.fetch_field(changeset, :device_id) do
      {:data, nil} ->
        Changeset.change(changeset,
          device_id: Polyjuice.Util.Identifiers.DeviceIdentifier.generate()
        )

      _ ->
        changeset
    end
  end

  @doc """
  Except a changeset to contain data coming from a client request. If it contains a `user_name` field, a new change is added with a generated userID.
  """
  def generate_username(changeset) do
    case Ecto.Changeset.fetch_field(changeset, :username) do
      {:data, nil} ->
        Ecto.Changeset.change(changeset,
          username: Polyjuice.Util.Identifiers.V1.UserIdentifier.generate_localpart()
        )

      _ ->
        changeset
    end
  end
end
