# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.V17.Controllers.RegisterController do
  use ExagonWeb, :controller
  alias Polyjuice.Util.ClientAPIErrors

  @register_default_kind "user"
  defmodule RegisterRequest do
    use Ecto.Schema
    @register_default_inhibit_login false
    @type t :: %__MODULE__{
            device_id: String.t(),
            inhibit_login: boolean(),
            initial_device_display_name: String.t(),
            password: String.t(),
            refresh_token: boolean(),
            username: String.t()
          }

    embedded_schema do
      field :device_id, :string
      field :inhibit_login, :boolean, default: @register_default_inhibit_login
      field :initial_device_display_name, :string
      field :password, :string
      field :refresh_token, :boolean
      field :username, :string
    end

    @spec from_params(params :: map()) ::
            {:ok, RegisterRequest.t()} | {:error, Ecto.Changeset.t()}
    def from_params(params) do
      params |> changeset() |> Ecto.Changeset.apply_action(:insert)
    end

    def changeset(params) do
      %RegisterRequest{}
      |> Ecto.Changeset.cast(params, [
        :device_id,
        :inhibit_login,
        :initial_device_display_name,
        :password,
        :refresh_token,
        :username
      ])
      |> ExagonWeb.Matrix.V17.Client.RequestUtils.generate_device_id()
      |> ExagonWeb.Matrix.V17.Client.RequestUtils.generate_username()
    end
  end

  def available(%{params: %{"username" => username}} = conn, _opts) do
    case Exagon.Users.is_username_available(username) do
      {:ok, true} ->
        conn |> json(%{"available" => true})

      {:ok, false} ->
        raise ExagonWeb.MatrixError,
          message: "Username '#{username}' is already in use",
          matrix_error: ClientAPIErrors.MUserInUse,
          plug_status: 400

      _ ->
        raise ExagonWeb.MatrixError,
          message: "Invalid username '#{username}'",
          matrix_error: ClientAPIErrors.MInvalidUsername,
          plug_status: 400
    end
  end

  def available(conn, _opts) do
    conn |> json(ClientAPIErrors.MBadJson.new()) |> halt
  end

  @spec register(Plug.Conn.t(), any) :: Plug.Conn.t()
  def register(conn, _opts) do
    with :ok <- validate_registration_enabled(conn),
         {:ok, kind} <- validate_kind(conn),
         {:ok, params} <- RegisterRequest.from_params(conn.params),
         device_info <- conn.assigns[:device_info],
         {:ok, %{user: user, device: device, account: _account, access_token: access_token}} <-
           Exagon.Users.register(kind, params, device_info) do
      response =
        case params.inhibit_login do
          false ->
            %{
              access_token: access_token.value,
              device_id: device.mx_device_id,
              expires_in_ms:
                DateTime.diff(access_token.expires_at, DateTime.utc_now(), :millisecond),
              refresh_token: access_token.refresh_token,
              user_id: user.mx_user_id
            }

          true ->
            %{user_id: user.mx_user_id}
        end

      conn |> json(response)
    else
      {:error, :m_invalid_username} ->
        raise ExagonWeb.MatrixError,
          message: "Invalid username",
          matrix_error: ClientAPIErrors.MInvalidUsername,
          plug_status: 400

      {:error, :m_user_in_use} ->
        raise ExagonWeb.MatrixError,
          message: "Username already used",
          matrix_error: ClientAPIErrors.MUserInUse,
          plug_status: 400

      {:error, :unexpected} ->
        raise ExagonWeb.MatrixError,
          message: "Internal error",
          matrix_error: ExagonWeb.Matrix.V17.Client.Errors.ExInternalError,
          plug_status: 500

      {:error, :registration_disabled} ->
        raise ExagonWeb.MatrixError,
          message: "Registration disabled",
          matrix_error: ClientAPIErrors.MForbidden,
          plug_status: 403

      {:error, :invalid_kind} ->
        raise ExagonWeb.MatrixError,
          message: "Invalid kind value",
          matrix_error: ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, %Ecto.Changeset{} = changeset} ->
        raise ExagonWeb.MatrixError,
          message: "Invalid request parameter: #{inspect(changeset)}",
          matrix_error: ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, :unexpected} ->
        raise ExagonWeb.MatrixError,
          message: "Unexpected error",
          matrix_error: ExagonWeb.Matrix.V17.Client.Errors.ExInternalError,
          plug_status: 500
    end
  end

  defp validate_registration_enabled(_conn) do
    case Exagon.Config.get_in_config(Matrix, [:registration, :enabled]) do
      true -> :ok
      _ -> {:error, :registration_disabled}
    end
  end

  defp validate_kind(%{params: params}) do
    case params["kind"] do
      kind when kind in ["user", "guest"] -> {:ok, kind}
      nil -> {:ok, @register_default_kind}
      _ -> {:error, :invalid_kind}
    end
  end
end
