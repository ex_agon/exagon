# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.V17.Controllers.KeysController do
  use ExagonWeb, :controller
  alias Polyjuice.Util.ClientAPIErrors
  alias Exagon.Matrix.Client.V17.DeviceKeys
  alias Exagon.Matrix.Client.V17.OneTimeKey

  def upload(%{params: params} = conn, _opts) do
    logged_in_user_id = ExagonWeb.Matrix.Client.ConnUtils.get_user_id(conn)
    logged_in_device_id = ExagonWeb.Matrix.Client.ConnUtils.get_device_id(conn)

    with %{"device_keys" => device_keys} when not is_nil(device_keys) <- params,
         {:ok, device_keys} <-
           DeviceKeys.from_params(device_keys, logged_in_user_id, logged_in_device_id),
         {:ok, device_key} <-
           Exagon.Users.upload_device_keys(device_keys, logged_in_user_id, logged_in_device_id) do
      device_key
    else
      {:error, :invalid_signature} ->
        raise ExagonWeb.MatrixError,
          message: "invalid device_keys signatures",
          matrix_error: ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, :key_not_found} ->
        raise ExagonWeb.MatrixError,
          message: "No sign key found for device #{logged_in_device_id}",
          matrix_error: ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, :unexpected} ->
        raise ExagonWeb.MatrixError,
          message: "Unexpected error",
          matrix_error: ExagonWeb.Matrix.V17.Client.Errors.ExInternalError,
          plug_status: 500
    end

    with %{"one_time_keys" => one_time_keys} when not is_nil(one_time_keys) <- params do
      for one_time_key <- one_time_keys do
        case one_time_key do
          key when is_map(key) ->
            OneTimeKey.from_params(key)

          key when is_binary(key) ->
            nil
        end
      end
    end

    conn |> send_resp(200, "")
  end
end
