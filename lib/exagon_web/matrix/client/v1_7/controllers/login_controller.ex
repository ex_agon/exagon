# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.Client.V17.Controllers.LoginController do
  use ExagonWeb, :controller
  alias Polyjuice.Util.ClientAPIErrors
  alias ExagonWeb.Matrix.Client.V17.Controllers.LoginController.LoginRequest

  def get_login(conn, _opts) do
    conn |> json(%{"flows" => [%{"type" => "m.login.password"}]})
  end

  def login(conn, _opts) do
    with {:ok, params} <- LoginRequest.from_params(conn.params),
         device_info <- conn.assigns[:device_info],
         {:ok, {user, _account, device, access_token}} <- Exagon.Users.login(params, device_info) do
      conn
      |> json(%{
        access_token: access_token.value,
        device_id: device.mx_device_id,
        expires_in_ms: DateTime.diff(access_token.expires_at, DateTime.utc_now(), :millisecond),
        refresh_token: access_token.refresh_token,
        user_id: user.mx_user_id,
        well_known: Exagon.Config.well_known()
      })
    else
      {:ok, :forbidden} ->
        raise ExagonWeb.MatrixError,
          message: "Access denied",
          matrix_error: ClientAPIErrors.MForbidden,
          plug_status: 403

      {:error, :unsupported_login_type} ->
        raise ExagonWeb.MatrixError,
          message: "Unsupported login type",
          matrix_error: Polyjuice.Util.ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, :invalid_identifier} ->
        raise ExagonWeb.MatrixError,
          message: "Invalid identifier",
          matrix_error: ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, %Ecto.Changeset{} = changeset} ->
        raise ExagonWeb.MatrixError,
          message: "Invalid request parameter: #{inspect(changeset)}",
          matrix_error: ClientAPIErrors.MBadJson,
          plug_status: 400

      {:error, :unexpected} ->
        raise ExagonWeb.MatrixError,
          message: "Unexpected error",
          matrix_error: ExagonWeb.Matrix.V17.Client.Errors.ExInternalError,
          plug_status: 500
    end
  end

  defmodule LoginRequest do
    use Ecto.Schema

    @type t :: %__MODULE__{
            device_id: String.t(),
            identifier: map(),
            initial_device_display_name: String.t(),
            password: String.t(),
            refresh_token: boolean(),
            token: String.t(),
            type: String.t()
          }

    embedded_schema do
      field :device_id, :string
      field :identifier, :map
      field :initial_device_display_name, :string
      field :password, :string, default: ""
      field :refresh_token, :boolean
      field :token, :string
      field :type, :string
    end

    @spec from_params(params :: map()) ::
            {:ok, LoginRequest.t()} | {:error, Ecto.Changeset.t()}
    def from_params(params) do
      params |> changeset() |> Ecto.Changeset.apply_action(:insert)
    end

    def changeset(params) do
      %LoginRequest{}
      |> Ecto.Changeset.cast(params, [
        :device_id,
        :identifier,
        :initial_device_display_name,
        :password,
        :refresh_token,
        :token,
        :type
      ])
      |> ExagonWeb.Matrix.V17.Client.RequestUtils.generate_device_id()
      |> Ecto.Changeset.validate_required(:type, message: "invalid_login_type")
      |> Ecto.Changeset.validate_inclusion(:type, [
        "m.login.password",
        "m.login.token",
        message: "invalid_login_type"
      ])
    end
  end
end
