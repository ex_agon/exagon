# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonWeb.Matrix.V17.Client.LastSeenPlug do
  require Logger
  use ExagonWeb, :controller
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(%{assigns: %{user_info: user_info}} = conn, _opts) do
    user_info |> Logger.info("Last seen: #{user_info}")
    conn
  end

  def call(conn, _opts) do
    conn
  end
end
