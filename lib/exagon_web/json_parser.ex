# Copyright 2022 Exagon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExgonWeb.JSONParser do
  @moduledoc """
  Parses JSON request body.

  Mostly code code coming from [https://github.com/elixir-plug/plug/blob/main/lib/plug/parsers/json.ex],
  but with Matrix spec specificities like
  [content type header being not mandatory](https://spec.matrix.org/v1.7/client-server-api/#api-standards).
  """
  @behaviour Plug.Parsers

  @impl true
  def init(opts) do
    {decoder, opts} = Keyword.pop(opts, :json_decoder)
    {body_reader, opts} = Keyword.pop(opts, :body_reader, {Plug.Conn, :read_body, []})
    decoder = validate_decoder!(decoder)
    {body_reader, decoder, opts}
  end

  defp validate_decoder!(nil) do
    raise ArgumentError, "JSON parser expects a :json_decoder option"
  end

  defp validate_decoder!({module, fun, args} = mfa)
       when is_atom(module) and is_atom(fun) and is_list(args) do
    arity = length(args) + 1

    if Code.ensure_compiled(module) != {:module, module} do
      raise ArgumentError,
            "invalid :json_decoder option. The module #{inspect(module)} is not " <>
              "loaded and could not be found"
    end

    if not function_exported?(module, fun, arity) do
      raise ArgumentError,
            "invalid :json_decoder option. The module #{inspect(module)} must " <>
              "implement #{fun}/#{arity}"
    end

    mfa
  end

  defp validate_decoder!(decoder) when is_atom(decoder) do
    validate_decoder!({decoder, :decode!, []})
  end

  defp validate_decoder!(decoder) do
    raise ArgumentError,
          "the :json_decoder option expects a module, or a three-element " <>
            "tuple in the form of {module, function, extra_args}, got: #{inspect(decoder)}"
  end

  @impl true
  def parse(conn, _type, _subtype, _headers, {{mod, fun, args}, decoder, opts}) do
    apply(mod, fun, [conn, opts | args]) |> decode(decoder, opts)
  end

  defp decode({:ok, "", conn}, _decoder, _opts) do
    {:ok, %{}, conn}
  end

  defp decode({:ok, body, conn}, {module, fun, args}, opts) do
    nest_all = Keyword.get(opts, :nest_all_json, false)

    try do
      apply(module, fun, [body | args])
    rescue
      e -> raise Plug.Parsers.ParseError, exception: e
    else
      terms when is_map(terms) and not nest_all ->
        {:ok, terms, conn}

      terms ->
        {:ok, %{"_json" => terms}, conn}
    end
  end

  defp decode({:more, _, conn}, _decoder, _opts) do
    {:error, :too_large, conn}
  end

  defp decode({:error, :timeout}, _decoder, _opts) do
    raise Plug.TimeoutError
  end

  defp decode({:error, _}, _decoder, _opts) do
    raise Plug.BadRequestError
  end
end
