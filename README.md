# Exagon

Exagon is an open-source Matrix homeserver implementation built to be resilient, performant and simple to maintain and administer.
## What is Matrix?

[Matrix](https://matrix.org/docs/chat_basics/matrix-for-im/#what-is-it) is an open network for secure and decentralized communication. Users from various Matrix homeserver can exchange information (like chat) with users from all other Matrix servers. 

Matrix is based on an [open-source protocol](https://spec.matrix.org/latest/) implemented by Synaspe, the current reference implementations. [Other implementation exist](https://www.matrix.org/ecosystem/servers/) and Exagon aims to be one of them.

## What is Exagon ?

Exagon implements all the specifications required by a Matrix homeserver (currently [v1.7](https://spec.matrix.org/v1.7/)):

 - Client-Server API
 - Server-Server API
 - Application Service API
 - Identity Service API
 - Push Gateway API

See [CHANGELOG] for details concerning implementation status of each part of these specification.

## How it works ?

Exagon is built with the [Phoenix framework](https://www.phoenixframework.org) which relies on [Elixir language](https://elixir-lang.org) and the BEAM Erlang virtual machine. This architecture provides Exagon a resilient and performant architecture.

## Contacts

Get in touch by joining the `#exagon:matrix.org` room on Matrix.