defmodule Exagon.Repo.Migrations.AddDeviceTable do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add(:mx_device_id, :text, null: false)
      add(:display_name, :text)
      add(:user_id, references(:users), null: false)
      add(:device_info, :map)
      timestamps()
    end
  end
end
