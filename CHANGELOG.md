# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Implement User-interactive authentication
- Implement `POST /_matrix/client/v3/register`. TBD: broadcast registration to other components.
- Implement `POST /_matrix/client/v3/login`.
- Implement `GET /_matrix/client/v3/login`.
- Implement `POST /_matrix/client/v3/register/available`
- Implement R0 endpoints
